// var filters = document.getElementById('checkeds');
//
// filters.addEventListener('click', function () {
//
// }, false);

function filters(id) {
    if (localStorage.getItem('filter-'+id)){
        localStorage.removeItem('filter-'+id);
    //     localStorage.setItem('filter-' + id, id);
    } else {
        localStorage.clear();
        localStorage.setItem('filter-' + id, id);
    }
    send_ajax();

}

function send_ajax() {
      // Объявляем AJAX запрос
    const xhr = new XMLHttpRequest();
    // Указываем метод и url адресс к которому будет привязан наш запрос
    let url = '/filter/?filter=';
    let act = false;
    for (let i of document.getElementsByName('filter')){
        if (localStorage.getItem('filter-'+ i.value)) {
            act = true;
            url += localStorage.getItem('filter-' + i.value)+','
        }
    }
    if (act == true) {
        xhr.open('GET', url, true);
        // Указываем заголовки
        xhr.setRequestHeader('Content-Type', 'application/json');
        xhr.onreadystatechange = function () {
            if (this.readyState != 4) return;

            if (this.status != 200) {
                // обработать ошибку
                console.error('Ошибка ajax');
                return;
            } else {
                render(JSON.parse(this.responseText));
            }
        };

        xhr.send();
    } else {
        window.location.replace("/");
    }
}

function render(dicts) {
    let contents = document.getElementById('publications');
    for (let ch of contents.children){
        ch.remove()
    }

    if (document.getElementById('publications').children.length > 0)
        document.getElementById('publications').children[0].remove();

    let bodys = "<div>";

    for (let items of dicts){
        if (items['id']) {
            bodys += "<h3><a href='publication/" + items.id + "'>" + items.header + "</a></h3>" +
                "<header class='post_meta'> <a href='#' class='' title='Автор публикации'> " +
                "<img src='"+ items.ava+" ' width='24' height='24' class='user-info__image'>" +
                "<span class='nikname'>"+ items.public_user +"</span></a>"+
                "</header>"+
                "<p>" +items.content+"</p></div>"
        }

    }
    bodys += "</div>";
    contents.innerHTML += bodys;



}