from django.urls import path

from .views import *

urlpatterns = [
    path('', Publications.as_view(), name="Publications"),
    path('filter/',Filters.as_view(), name='Filters'),
    path('sections/', Sections.as_view(), name='Sections'),
    path('publication/<slug:pk>', Publication_number.as_view(), name='Publication_number'),
    path('search/', Search.as_view(), name='Search')
]
