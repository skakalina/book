from django.db import models
from django.contrib.auth.models import AbstractBaseUser
from ckeditor_uploader.fields import RichTextUploadingField


class Account(AbstractBaseUser):
    first_name = models.CharField('Имя', max_length=50, unique=False, null=False, blank=False)
    last_name = models.CharField('Фамилия', max_length=50, unique=False, null=False, blank=False)
    nikname = models.CharField('Никнейм', max_length=100, unique=False, null=False, blank=False)
    password = models.CharField('Пароль', max_length=100, null=False, blank=False)
    ava = models.ImageField('Добавить фото', upload_to='ava', null=True, blank=True, default='/ava/smile.png')
    email = models.EmailField('Электронная почта', unique=True, null=False, blank=False, max_length=150)
    city = models.CharField('Город', max_length=100, null=True, blank=True)
    about_oneself = models.TextField('О себе', null=True, blank=True)

    USERNAME_FIELD = 'email'  # Вход
    REQUIRED_FIELDS = ['nikname', 'email', 'first_name']  # Обязательные поля

    def __str__(self):
        return self.nikname

class Note(models.Model):
    name_note = models.CharField('Назвние заметки', max_length=210, blank=False, null=False, )
    content = models.CharField('Содеражание', max_length=1000, blank=False, null=False)
    user_notes = models.ForeignKey(Account, blank=True, null=True, on_delete=models.CASCADE)

    def __str__(self):
        return self.name_note


# Create your models here.
class Section(models.Model):
    section = models.CharField('Название раздела', max_length=100, unique=True, blank=False, null=False)

    def __str__(self):
        return self.section


class Publication(models.Model):
    header = models.CharField('Название статьи', max_length=350, blank=False, null=False, db_index=True)
    content = RichTextUploadingField(db_index=True)
    publication = models.BooleanField(default=False)
    public_user = models.ForeignKey(Account, on_delete=models.CASCADE, blank=True, null=True)
    publication_section = models.ForeignKey(Section, on_delete=models.CASCADE, blank=True, null=True)

    def __str__(self):
        return self.header
