from django.contrib import admin
from .models import Account, Note, Publication, Section


admin.site.register(Account)
admin.site.register(Note)
admin.site.register(Publication)
admin.site.register(Section)

# Register your models here.
