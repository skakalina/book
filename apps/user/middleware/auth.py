from django.utils.deprecation import MiddlewareMixin
from django.http import HttpResponseRedirect
# Промежуточный запрос
class AuthNew(MiddlewareMixin):
    # То что происходит при GET запросе
    def process_request(self,request):
        print(request.session.get('auth'))
        # Если человек авторизован и находится на странице авторизации, то делаем редирект
        if (('/user/auth' in request.path) or ('/user/reg' in request.path)) and (request.session.get('auth')):
            return HttpResponseRedirect('/')
        elif (('/user/auth' in request.path) or ('/user/reg' in request.path) and (request.session.get('auth')==None)):
            pass
        elif ((request.path.find('/user') != -1) and (request.session.get('auth')==None)):
            return HttpResponseRedirect('/')