# Generated by Django 2.0.4 on 2018-05-13 11:07

import ckeditor_uploader.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('user', '0009_auto_20180510_1658'),
    ]

    operations = [
        migrations.AlterField(
            model_name='publication',
            name='content',
            field=ckeditor_uploader.fields.RichTextUploadingField(db_index=True),
        ),
        migrations.AlterField(
            model_name='publication',
            name='header',
            field=models.CharField(db_index=True, max_length=350, verbose_name='Название статьи'),
        ),
    ]
