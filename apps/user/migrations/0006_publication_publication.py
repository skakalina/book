# Generated by Django 2.0.4 on 2018-05-08 19:47

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('user', '0005_publication'),
    ]

    operations = [
        migrations.AddField(
            model_name='publication',
            name='publication',
            field=models.BooleanField(default=False),
        ),
    ]
