from django.urls import path

from .views import *

urlpatterns = [
    path('reg/', Reg.as_view(), name="Reg"),
    path('auth/', Auth.as_view(), name="Auth"),
    path('logout/', Auth.as_view(), name="Logout"),
    path('notes/', Notes.as_view(), name='Notes'),
    path('note/', Note_edit.as_view(), name='Note_edit'),
    path('note/del/<slug:pk>', DelNote.as_view(), name='DelNote'),
    path('publications/', Publications.as_view(), name='Publications'),
    path('publication/create/', Public.as_view(), name='Public'),
    path('not_published/', Not_published.as_view(), name='Not_published'),
    path('settings/', Setting.as_view(), name='Setting'),
    path('publication/<slug:pk>/', Publication_exit.as_view(), name='Publication_exit'),
]
