class A:
    def get(self):
        print(2)


class B(A):

    def get(self):
        super(B, self).get()
        print(15)

    def post(self):
        print(3)
####
B().get()
B().post()


A().get()