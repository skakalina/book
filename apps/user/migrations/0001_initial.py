# Generated by Django 2.0.4 on 2018-05-04 18:57

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Account',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('last_login', models.DateTimeField(blank=True, null=True, verbose_name='last login')),
                ('first_name', models.CharField(max_length=50, verbose_name='Имя')),
                ('last_name', models.CharField(max_length=50, verbose_name='Фамилия')),
                ('nikname', models.CharField(max_length=100, unique=True, verbose_name='Никнейм')),
                ('password', models.CharField(max_length=100, verbose_name='Пароль')),
                ('ava', models.ImageField(upload_to='user', verbose_name='Добавить фото')),
                ('email', models.EmailField(max_length=150, unique=True, verbose_name='Электронная почта')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
