from rest_framework import serializers
from .models import Publication

class Publics(serializers.ModelSerializer):

    class Meta:
        model = Publication
        fields = ('__all__')