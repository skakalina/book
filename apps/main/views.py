import json

from django.shortcuts import render
from django.views.generic import ListView, DetailView, View
from django.http import HttpResponse
from .models import *
from apps.user.models import Publication, Section
from django.contrib.postgres.search import TrigramSimilarity
from django.db.models.functions import Greatest

class Filters(View):
    def get(self,request,**kwargs):
        if 'filter' in self.request.GET:
            filters = self.request.GET['filter'][0:-1].split(',')
            res = Publication.objects.filter(publication=True,publication_section__in=filters)
            if len(res) > 0:
                filt = []
                for i in res:
                    try:
                        url = i.public_user.ava.url
                    except:
                        url = '/media'
                    filt.append({
                        'id': i.id,
                        'header': i.header,
                        'content': i.content,
                        'public_user': i.public_user.nikname,
                        'ava': url,
                        'publication_section': i.publication_section.section
                    })
                return HttpResponse(json.dumps(filt))
            else:
                return HttpResponse(list({'get': 'empty'}))
        else:
            return HttpResponse(list({'get':'empty'}))

# Create your views here.
class Publications(ListView):
    model = Publication
    template_name = 'home.html'

    def get_queryset(self):
            return self.model.objects.filter(publication=True).order_by('-id')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section'] = Section.objects.all()
        return context

class Sections(ListView):
    model = Section
    template_name = 'sections.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context

class Publication_number(DetailView):
    model = Publication
    template_name = 'publication_number.html'

    def get_context_data(self, **kwargs):
        next = super().get_context_data(**kwargs)
        return next

class Search(ListView):
    model = Publication
    template_name = 'home.html'

    def get_queryset(self):
        # Полнотекстовый поиск
        # similarity - поиск по "неполному" совпадению
        # similarity__gt - процент совпадения где 1 это 100%
        # TrigramSimilarity - метод поиска
        if 'search' in self.request.GET:
            return self.model.objects.annotate(similarity=Greatest(
                TrigramSimilarity('header', self.request.GET['search']),
                TrigramSimilarity('content', self.request.GET['search']),
            )).filter(similarity__gt=0.1,publication=True).order_by('-id')
        else:
            return []
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section'] = Section.objects.all()
        return context