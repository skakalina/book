from django.views.generic import DetailView, ListView, CreateView, TemplateView
from django.http import HttpResponseRedirect
from .models import Account, Note, Publication, Section
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from .serializers import Publics


class Publics1(APIView):

    def get(self,request):
        # Publication.create(name=requst.GET['name'])
        # a = Publication(name='as')
        # a.save()
        publuc1=Publication.objects.all()
        serializer= Publics(publuc1, many=True)
        return Response(serializer.data)

    def post(self):
        pass


class Reg(CreateView):
    model = Account
    fields = ['first_name', 'last_name', 'nikname', 'password', 'email', 'ava']
    template_name = 'profile/reg.html'
    success_url = '/'


class Auth(TemplateView):
    template_name = 'profile/login.html'

    def post(self, request):
        auth = Account.objects.get(email=request.POST['email'], password=request.POST['password'])
        if auth:
            # Создание сессии
            self.request.session['auth'] = str(auth.id)
            return HttpResponseRedirect('/')
        else:
            return False

    def get(self, request, *args, **kwargs):
        context = super(Auth, self).get(request, **kwargs)
        if self.request.path == '/user/logout/':
            if 'auth' in self.request.session:
                del self.request.session['auth']
            return HttpResponseRedirect('/')
        return context

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context


class Notes(ListView):
    model = Note # = Note.objects.all()
    template_name = 'notes/notes.html'

    def get_queryset(self):
        return self.model.objects.filter(user_notes_id=self.request.session['auth'])

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context


class Note_edit(TemplateView):
    template_name = 'notes/note.html'

    def get(self, request, **kwargs):
        context = super(Note_edit, self).get(request, **kwargs)
        if ('name_note' in request.GET) and ('content' in request.GET):
            Note.objects.create(name_note=request.GET['name_note'], content=request.GET['content'], user_notes_id=self.request.session['auth'])
            # a = Note.objects.get(id=1)
            # a.delete()
            return HttpResponseRedirect('/user/notes/')
        return context


class DelNote(TemplateView):
    template_name = 'notes/note.html'

    def get(self, request, **kwargs):
        context = super(DelNote, self).get(request, **kwargs)
        Note.objects.get(id=self.kwargs['pk']).delete()
        return HttpResponseRedirect('/user/notes/')


class Publications(ListView):
    model = Publication
    template_name = 'publications/publications.html'

    def get_queryset(self):
        return self.model.objects.filter(publication=True, public_user_id=self.request.session['auth'])

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context


class Public(TemplateView):
    template_name = 'publications/hye.html'

    def get(self, request, **kwargs):
        if ('header' in request.GET) and ('content' in request.GET):
            sections = Section.objects.filter(section=request.GET['section'])[0]
            Publication(header=request.GET['header'], content=request.GET['content'],
                        public_user_id=int(request.session['auth']), publication_section_id=sections.id).save()
            return HttpResponseRedirect('/user/publications')
        context = super().get(request, **kwargs)
        return context

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section'] = Section.objects.filter()
        return context


class Not_published(ListView):
    model = Publication
    template_name = 'publications/not_published.html'

    def get_queryset(self):
        return self.model.objects.filter(publication=False, public_user_id=self.request.session['auth'])

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context


class Setting(TemplateView):
    template_name = 'profile/settings.html'

    def post(self, request):
        if 'ava' in request.FILES:
            if request.FILES['ava']:
                auth = Account(id=int(request.session['auth']), email=request.POST['email'],
                               first_name=request.POST['first_name'],last_name=request.POST['last_name'],
                               nikname=request.POST['nikname'], ava=request.FILES['ava'],
                               city=request.POST['city'], about_oneself=request.POST['about_oneself'], password=request.POST['password']).save()
                return HttpResponseRedirect('/')

        auth = Account(id=int(request.session['auth']), email=request.POST['email'],
                       first_name=request.POST['first_name'], last_name=request.POST['last_name'],
                       nikname=request.POST['nikname'], city=request.POST['city'],
                       about_oneself=request.POST['about_oneself'], password=request.POST['password']).save()
        return HttpResponseRedirect('/')


    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['user'] = Account.objects.get(id=int(self.request.session['auth']))
        return context


class Publication_exit(DetailView):
    model = Publication
    template_name = 'publications/publication.html'

    def get_context_data(self, **kwargs):
        news = super().get_context_data(**kwargs)

        return news
